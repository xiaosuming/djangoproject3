from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, filters
from apps.application.models import Application
from apps.application.serializers import ApplicationSerializer
from django_filters import rest_framework as filters


class ApplicationFilter(filters.FilterSet):  # 筛选过滤器
    """应用过滤器"""

    name= filters.CharFilter(field_name="name",lookup_expr="icontains")  # 自己的字段
    class Meta:
        model = Application
        fields = ('name',)


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='返回用户可安装/修改的应用(默认10条每页)',
    manual_parameters=[
        openapi.Parameter('name', openapi.IN_QUERY, description='应用名称',
                          type=openapi.TYPE_STRING),
    ]
))

@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='新的应用',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, description="新的应用",
        required=['name', 'user'],
        properties={
            'name': openapi.Schema(
                type=openapi.TYPE_STRING, description='name'),
            'user': openapi.Schema(
                type=openapi.TYPE_INTEGER, description='user'),
        }, )

))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='返回单个应用的详情'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='更新应用'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='更新应用指定的属性',
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='移除应用'
))


class ApplicationViewSet(viewsets.ModelViewSet):
    """
    允许用户查看。
    """
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    filterset_class = ApplicationFilter

