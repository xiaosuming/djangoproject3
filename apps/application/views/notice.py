from rest_framework import viewsets
from apps.application.models import Notice
from apps.application.serializers import NoticeSerializer


class NoticeViewSet(viewsets.ModelViewSet):

    queryset = Notice.objects.all()
    serializer_class = NoticeSerializer
