from django.http import HttpResponse, JsonResponse
from django.views import View
from rest_framework.response import Response
from apps.application.models import Application


class UploadIcon(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def post(self,request):

        '''上传头像'''
        application_id = request.POST.get('application_id')  # 获取是否已读参数
        icon = request.FILES.get('icon')  # 获取头像名称
        application = Application.objects.get(id=application_id)

        # 1. 删除原头像
        application.icon.delete()

        # 2. 将传来的头像数据，保存到数据库
        application.icon = icon
        application.save()


        # 3. 拼接图片的路径
        icon_addr = application.get_icon_url()
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "data": icon_addr
        }

        return JsonResponse(res_data)



