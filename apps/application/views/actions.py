from rest_framework import viewsets
from apps.application.models import  Actions
from apps.application.serializers import  ActionsSerializer


class ActionsViewSet(viewsets.ModelViewSet):

    queryset = Actions.objects.all()
    serializer_class = ActionsSerializer
