from django.contrib.auth.models import User
from django.db import models

MEDIA_ADDR = 'http://127.0.0.1:8000/media/'



class Application(models.Model):
    """应用模型"""
    name = models.CharField(max_length=255, verbose_name=u'应用名字', default="")
    icon = models.ImageField(blank=True, verbose_name=u"应用图标", upload_to="icon", default='icon/2020-11-06_16-43-28_çš„å±å¹•æˆªå›¾.png')
    user = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                             related_name='user_apps', blank=True, null=True)
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)

    class Meta:
        verbose_name = u'应用列表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.name)

    def get_icon_url(self):
        '''返回头像的url'''
        return MEDIA_ADDR + str(self.icon)



class Task(models.Model):
    """任务模型"""
    name = models.CharField(max_length=255, verbose_name=u'任务名', default="", )
    url = models.URLField(max_length=200, null=True, )
    img = models.ImageField(blank=True, verbose_name=u"图片", upload_to="img/", default='')
    receiver = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                             related_name='receive_tasks', blank=True, null=True)
    sender = models.ForeignKey(to=User, on_delete=models.CASCADE, verbose_name=u'发送者', related_name='send_tasks', blank=True, null=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='app_tasks', verbose_name=u'所属应用', default='')
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)

    class Meta:
        verbose_name = u'展示任务'
        verbose_name_plural = verbose_name

    def get_img_url(self):
        '''返回头像的url'''
        return MEDIA_ADDR + str(self.img)

    def __str__(self):
        return u'%s' % (self.name)


class Notice(models.Model):
    """应用通知模型"""
    SENDER_TYPE = (("system", "系统消息"), ("user", "用户消息"))
    REMIND_TYPE = (("week", "弱提醒"), ("strong", "强提醒"))
    NOTICE_ACTION = (("accept", "接受"), ("strong", "拒绝"), (" ", "待定"))
    APPLY_TYPE = (("friend_apply", "好友申请"), ("groupchat_apply", "入群申请"))
    NOTICE_TYPE = (("notice", "通知"), ("task", "任务"), ("comment", "评论"), ("apply", "好友申请"))
    REQUEST_TYPE = (("post", "新增"), ("get", "查看"), ("delete", "删除"), ("patch", "局部修改"), ("put", "整体修改"))

    sender = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='send_notices',verbose_name=u'发送者', blank=True, null=True)
    receiver = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="",
                             related_name='receive_notices', blank=True, null=True)
    notice_content = models.TextField(verbose_name=u'通知内容 ')
    meta = models.CharField(max_length=255, default="{}", verbose_name="请求参数", null=True)
    notice_type = models.CharField(max_length=32, choices=NOTICE_TYPE, verbose_name="消息类型", default='', null=True)
    remind_type = models.CharField(max_length=32, choices=REMIND_TYPE, verbose_name="提醒类型", default='', null=True)
    sender_type = models.CharField(max_length=32, choices=SENDER_TYPE, verbose_name="发送者类型", default='', null=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, blank=True, null=True,
                                    related_name='app_notices', verbose_name=u'所属应用', default='')
    tasks = models.ManyToManyField(Task, related_name='task_notices', verbose_name=u'所属任务', default='')
    isreaded = models.NullBooleanField(choices=((True, u"已读"), (False, u"未读")), default=False, verbose_name=u"是否已读",
                                       blank=True, null=True)
    apply_type = models.CharField(max_length=32, choices=APPLY_TYPE, default='', verbose_name=u"消息申请类型",
                                  blank=True, null=True)
    perform_action = models.CharField(max_length=32, choices=NOTICE_ACTION, verbose_name="动作已执行状态", default='',
                                      null=True)
    istop = models.BooleanField(choices=((True, u"置顶"), (False, u"未置顶")), default=False, verbose_name=u"是否置顶")
    send_time = models.DateTimeField(verbose_name=u"发送时间", auto_now_add=True, null=True)

    class Meta:
        verbose_name = u'应用通知列表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s,%s' % (self.sender, self.notice_content)


class Actions(models.Model):
    """执行动作"""

    REQUEST_TYPE = (("post", "新增"), ("get", "查看"), ("delete", "删除"), ("patch", "局部修改"), ("put", "整体修改"))
    notice_action = models.CharField(max_length=32, verbose_name="执行动作", default='', null=True)
    action_type = models.CharField(max_length=32, verbose_name="动作类型", default='', null=True)
    method = models.CharField(max_length=32, choices=REQUEST_TYPE, verbose_name="请求方法", default='', null=True)
    request_url = models.URLField(max_length=200, null=True, verbose_name="文件链接")
    notice = models.ForeignKey(to=Notice, on_delete=models.CASCADE, related_name='notice_actions', verbose_name=u'所属消息',
                               blank=True, null=True)

    class Meta:
        verbose_name = u'执行动作'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.method)
