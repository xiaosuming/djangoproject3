from django.conf.urls import url
from rest_framework import routers
from apps.application import views
from apps.application.views.actions import ActionsViewSet
from apps.application.views.application import ApplicationViewSet
from apps.application.views.notice import NoticeViewSet
from django.urls import re_path


# 应用的url
from apps.application.views.uplode_icon import UploadIcon

application_routers = routers.DefaultRouter(trailing_slash=False)
application_routers.register(r'applications', ApplicationViewSet)
application_routers.register(r'notices', NoticeViewSet)
application_routers.register(r'actions', ActionsViewSet)
# application_urls = [re_path(r'^actions/(?P<action_id>\d+)$', ActionsView.as_view()),
#              ] + application_routers.urls

urlpatterns = [url(r'icons$',  UploadIcon.as_view())]















