from rest_framework import serializers
from apps.application.models import Application, Notice, Actions


class ApplicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        # fields = ( 'name','icon')
        fields = "__all__"


    def to_representation(self, instance):  # 展示给前端的替换和拼接，哪一个不想展现，pop剔除除去
        attribute = super().to_representation(instance)
        # name=attribute.pop("name")
        # user=attribute.pop("user")
        attribute["icon"] = instance.get_icon_url()
        return attribute




class NoticeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notice
        # fields = ( 'name','icon')
        fields = "__all__"



class ActionsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Actions
        # fields = ( 'name','icon')
        fields = "__all__"