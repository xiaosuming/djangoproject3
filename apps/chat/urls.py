from rest_framework import routers
from apps.chat.views.enclosure import EnclosureViewSet
from apps.chat.views.friend import FriendViewSet
from apps.chat.views.group import GroupViewSet
from apps.chat.views.message import MessageViewSet
from apps.chat.views.users import  UserViewSet

# 聊天的url
chat_routers = routers.DefaultRouter(trailing_slash=False)
chat_routers.register(r'groups', GroupViewSet)
chat_routers.register(r'messages', MessageViewSet)
chat_routers.register(r'friends', FriendViewSet)
chat_routers.register(r'enclosures', EnclosureViewSet)
chat_routers.register(r'users', UserViewSet)
# chat_urls = [re_path('myfriendslist', MyfriendView.as_view()),
#              re_path(r'^file_actions/(?P<action_id>\d+)$', All_ActionsView.as_view()),
#              ] + chat_routers.urls



















































