from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from apps.application.models import Task
from random import choice


MEDIA_ADDR = 'http://127.0.0.1:8000/'


class UserProfile(models.Model):
    """自定义用户模型"""

    PORTRAIT_CHOICES = (
        "#FE375F", "#C059F2", "#2A84FF",
        "#32D74B", "#5E5CE6", "#64D2FF",
        "#FD453A", "#FD9F0A", "#FDD50A"
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_profile')
    login_name = models.CharField(max_length=255, verbose_name=u'登录名', unique=True)
    colour = models.CharField(max_length=7, verbose_name=u'头像颜色', default="#4682B4")
    phone = models.CharField(max_length=255, verbose_name=u'电话')

    # @receiver(post_save, sender=User)
    # def create_user_profile(sender, instance, created, **kwargs):
    #     if created:
    #         User.objects.create(user=instance)
    #     else:
    #         instance.extension.save()

    def __str__(self):
        return self.login_name

    def get_color(self):
        return choice(self.PORTRAIT_CHOICES)

    class Meta:
        verbose_name = u'用户扩展'
        verbose_name_plural = verbose_name



class Group(models.Model):
    """群组模型"""
    name = models.CharField(max_length=255, verbose_name=u'群名', blank=True, null=True)
    creater = models.ForeignKey(to=User, null=True, blank=True, related_name='create_groups',
                                          on_delete=models.CASCADE, verbose_name=u"创建人")
    members = models.ManyToManyField(to=User, related_name='group_members', verbose_name=u'群成员', blank=True)
    is_group = models.NullBooleanField(verbose_name=u'群聊', choices=((False, '否'), (True, u'是')), default=False,
                                       blank=True, null=True)
    colour = models.CharField(max_length=7, verbose_name=u'头像颜色', null=True, )
    is_deleteed = models.NullBooleanField(verbose_name=u'删除', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)
    created_time = models.DateTimeField(verbose_name=u"创建时间", auto_now_add=True)

    class Meta:
        verbose_name = u'群组'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.name)


class Enclosure(models.Model):
    """附件模型"""

    ENCLOSURE_ACTION = (("accept", "接受"), ("strong", "拒绝"), (" ", "待定"))
    name = models.CharField(max_length=128, verbose_name=u'文件名', blank=True, null=True)
    size = models.IntegerField(default=0, null=True, blank=True, verbose_name=u'文件大小')
    file_type = models.CharField(max_length=128, verbose_name=u'文件类型', blank=True, null=True)
    path = models.CharField(max_length=256, verbose_name=u'存储路径', blank=True, null=True)
    md5 = models.CharField(max_length=32, verbose_name=u"文件唯一标识符", blank=True, null=True)
    colour = models.CharField(max_length=7, verbose_name=u'头像颜色', null=True)
    is_deleteed = models.NullBooleanField(verbose_name=u'清理', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)
    sender = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='enclosureuser',
                                       verbose_name=u'附件发送人', blank=True, null=True)
    Groups = models.ManyToManyField(to=Group, related_name='group_enclosures', verbose_name=u'附件所属群组',
                                        blank=True)
    perform_action = models.CharField(max_length=32, choices=ENCLOSURE_ACTION, verbose_name="动作已执行状态", default='',
                                      null=True)
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=u'创建时间')

    class Meta:
        verbose_name = u'附件'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u"%s" % (self.name,)

    def get_file_url(self):
        '''返回头像的url'''
        return MEDIA_ADDR + str(self.path)


class Message(models.Model):
    """消息模型"""
    MESSAGE_TYPE = (
        ("message_normal", "正常消息"),("message_groupcard", "群名片"), ("message_usercard", "用户名片"), ("message_task", "任务"),
        ("message_file", "文件"))
    sender = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='send_messages', verbose_name=u'发信人',
                             blank=True, null=True)
    text_content = models.TextField(blank=True, null=True, verbose_name=u'文本内容', default='')
    message_type = models.CharField(max_length=32, choices=MESSAGE_TYPE, verbose_name="消息类型", default="message_normal",
                                    blank=True, null=True)
    isreaded = models.NullBooleanField(choices=((True, u"已读"), (False, u"未读")), default=False, verbose_name=u"是否已读")
    istop = models.NullBooleanField(choices=((True, u"置顶"), (False, u"未置顶")), default=False, verbose_name=u"是否置顶")
    groupcard = models.ForeignKey(to=Group, null=True, blank=True, on_delete=models.CASCADE,related_name='group_message',
                                  verbose_name=u'名片对应的群的id')
    usercard = models.ForeignKey(to=User, null=True, blank=True, on_delete=models.CASCADE,related_name='user_message', verbose_name=u'名片对应的人的id')
    task = models.ForeignKey(to=Task, null=True, blank=True, on_delete=models.CASCADE, verbose_name=u'任务对应的任务的id')
    group = models.ForeignKey(to=Group, on_delete=models.CASCADE, related_name='group_messages',
                                  verbose_name=u"所属群组", blank=True, null=True)
    send_time = models.DateTimeField(verbose_name=u"发送时间", auto_now_add=True)

    class Meta:
        verbose_name = u'消息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s,%s' % (self.sender, self.text_content)


class Friend(models.Model):
    """我的好友模型"""
    user = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="", related_name='user_friend',
                              blank=True, null=True)
    friend = models.ForeignKey(to=User, verbose_name=u'好友详情', on_delete=models.CASCADE,
                                      related_name='friend_profile', default="", blank=True, null=True)
    remark = models.TextField(max_length=1024, blank=True, null=True, default='', verbose_name=u'备注')
    created_time = models.DateTimeField(auto_now_add=True, verbose_name=u'创建时间')

    class Meta:
        verbose_name = u'我的好友'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.friend)


class MessagePart(models.Model):
    """清空消息模型"""
    TYPE = (("normal", "正常清空"), ("out", "出群清空"), ("into", "入群清空"))
    user = models.ForeignKey(to=User, verbose_name=u'', on_delete=models.CASCADE, related_name='send_mes',
                             default="", blank=True, null=True)
    Group = models.ForeignKey(to=Group, verbose_name=u'所属群聊', on_delete=models.CASCADE,
                                  related_name='group_mes', default="", blank=True, null=True)
    last_message = models.ForeignKey(to=Message, verbose_name=u'最新消息', on_delete=models.CASCADE,
                                     default="", blank=True, null=True)
    type = models.CharField(max_length=32, choices=TYPE, verbose_name="清空的类型", default="normal",
                         blank=True, null=True)

    class Meta:
        verbose_name = u'清空消息记录'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)


class GroupPart(models.Model):
    """ 删除聊天列表模型 """
    user = models.ForeignKey(to=User, verbose_name=u'删除者', on_delete=models.CASCADE, related_name='delete_groups',
                             default="", blank=True, null=True)
    Group = models.ForeignKey(to=Group, verbose_name=u'所属群聊', on_delete=models.CASCADE,
                                  related_name='self_group', default="", blank=True, null=True)
    is_deleteed = models.NullBooleanField(verbose_name=u'是否彻底删除 ', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)
    last_message = models.ForeignKey(to=Message, verbose_name=u'删除聊天前最后一条消息', on_delete=models.CASCADE,
                                      default="", blank=True, null=True)

    class Meta:
        verbose_name = u'删除聊天列表模型'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)


class GroupMemberPart(models.Model):
    """ 删除群成员列表模型 """
    user = models.ForeignKey(to=User, verbose_name=u'删除的群成员', on_delete=models.CASCADE, related_name='user_memebers',
                             default="", blank=True, null=True)
    Group = models.ForeignKey(to=Group, verbose_name=u'所属群聊', on_delete=models.CASCADE,
                                  related_name='group_profile', default="", blank=True, null=True)
    is_deleteed = models.NullBooleanField(verbose_name=u'是否彻底删除 ', choices=((False, '否'), (True, u'是')), default=False,
                                      blank=True, null=True)

    class Meta:
        verbose_name = u'删除群成员列表模型'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)


class FriendPart(models.Model):
    """ 删除好友模型 """
    user = models.ForeignKey(to=User, verbose_name=u'所属人', on_delete=models.CASCADE, default="", related_name='user_friends',
                              blank=True, null=True)
    friend = models.ForeignKey(to=User, verbose_name=u'好友', on_delete=models.CASCADE,
                                 related_name='friend_profiles', default="", blank=True, null=True)

    class Meta:
        verbose_name = u'删除好友模型'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % (self.user)
