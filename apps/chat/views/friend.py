from rest_framework import viewsets
from apps.chat.models import Friend
from apps.chat.serializers import FriendSerializer


class FriendViewSet(viewsets.ModelViewSet):

    queryset = Friend.objects.all()
    serializer_class = FriendSerializer
