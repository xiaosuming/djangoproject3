from rest_framework import viewsets
from apps.chat.models import Message
from apps.chat.serializers import MessageSerializer


class MessageViewSet(viewsets.ModelViewSet):

    queryset = Message.objects.all()
    serializer_class = MessageSerializer
