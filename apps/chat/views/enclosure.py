from rest_framework import viewsets
from apps.chat.models import Enclosure
from apps.chat.serializers import EnclosureSerializer


class EnclosureViewSet(viewsets.ModelViewSet):
    """
    允许用户查看或编辑的API路径。
    """
    queryset = Enclosure.objects.all()
    serializer_class = EnclosureSerializer
