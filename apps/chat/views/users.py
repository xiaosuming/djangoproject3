from django.contrib.auth.models import User
from rest_framework import viewsets
# from apps.chat.models import UserProfile
from apps.chat.serializers import UserSerializer


# class UserProfileViewSet(viewsets.ModelViewSet):
#
#     queryset = UserProfile.objects.all()
#     serializer_class = UserProfileSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


