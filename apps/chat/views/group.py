from rest_framework import viewsets
from apps.chat.models import Group
from apps.chat.serializers import GroupSerializer

class GroupViewSet(viewsets.ModelViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
