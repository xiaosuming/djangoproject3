from django.contrib.auth.models import User
from django.db.transaction import atomic
from rest_framework import serializers
from apps.application.models import Application
from apps.chat.models import Group, Enclosure, Message, Friend, MessagePart, GroupPart, GroupMemberPart, \
    FriendPart, UserProfile


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        # fields = ( 'name','icon')
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):

    login_name = serializers.CharField(max_length=128, write_only=True,default="", help_text='名片')
    colour = serializers.CharField(max_length=128, write_only=True,default="", help_text='名片')
    phone = serializers.CharField(max_length=128, write_only=True,default="", help_text='名片')

    # user_id = serializers.CharField(max_length=128, default="", help_text='发送者')
    # login_name = serializers.CharField(max_length=128, write_only=True, default="[]", help_text='接收者')
    user_profile=UserProfileSerializer()

    # login_name = serializers.ReadOnlyField(source='user_profile.login_name',allow_null=True)
    # phone = serializers.ReadOnlyField(source='user_profile.phone',allow_null=True)

    class Meta:
        model = User
        # fields = "__all__"
        # fields = ( 'username','login_name','password','user_profile','phone')
        fields = ( 'id','username','password','login_name','colour','phone','user_profile')


    def create(self,validate_data):  # 对于新增的时候，需要增加第三张表，那么就需要在这里面重写
        print(validate_data)
        login_name = validate_data.pop('login_name')  # 这表示从上面的字段，把这些字段踢出来，用于后期的建立另外一张表
        colour= validate_data.pop('colour')  # 这表示从上面的字段，把这些字段踢出来，用于后期的建立另外一张表
        phone = validate_data.pop('phone')  # 这表示从上面的字段，把这些字段踢出来，用于后期的建立另外一张表

        with atomic():
            print(validate_data)
            user = super().create(validate_data)  # 建立新的消息
            if login_name:
                UserProfile.objects.create(login_name=login_name,phone=phone,user_id=user.id)
            else:
                pass
        return user





# class UserProfileSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = UserProfile
#         # fields = ( 'name','icon')
#         fields = "__all__"



class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        # fields = ( 'name','icon')
        fields = "__all__"


class EnclosureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Enclosure
        # fields = ( 'name','icon')
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        # fields = ( 'name','icon')
        fields = "__all__"


class FriendSerializer(serializers.ModelSerializer):

    class Meta:
        model = Friend
        # fields = ( 'name','icon')
        fields = "__all__"

#
# class MessagePartSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = MessagePart
#         # fields = ( 'name','icon')
#         fields = "__all__"
#
#
# class GroupPartSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = GroupPart
#         # fields = ( 'name','icon')
#         fields = "__all__"
#
# class GroupMemberPartSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = GroupMemberPart
#         # fields = ( 'name','icon')
#         fields = "__all__"
#
# class FriendPartSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = FriendPart
#         # fields = ( 'name','icon')
#         fields = "__all__"