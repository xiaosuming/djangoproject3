from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from apps.application.models import Application


def upload_icon(request):
    '''上传头像'''
    if request.method=="POST":
        icon = request.FILES.get('icon')  # 获取头像名称
        application = Application.objects.get(id=2)


        # 1. 删除原头像
        application.icon.delete()

        # 2. 将传来的头像数据，保存到数据库
        application.icon = icon
        application.save()

        # 3. 拼接图片的路径
        icon_addr = application.get_icon_url()
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "data": icon_addr
        }

        return JsonResponse(res_data)





