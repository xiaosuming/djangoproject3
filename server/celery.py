from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'server.settings')
app = Celery('server')
app.config_from_object('django.conf:settings', namespace='CELERY')

# 下面的设置就是关于调度器beat的设置
app.conf.beat_schedule = {
    'clear_message': {  # 消息超过90天，就进行删除
        'task': 'server.task.clear_message',  # 设置是要将哪个任务进行定时
        'schedule': crontab(minute=0, hour=12),  # 调用crontab进行具体时间的定义
        # 'schedule': 3,  # 调用crontab进行具体时间的定义
        'args':''
    },
    'clear_notice': {  # 通知超过90天，就进行删除
        'task': 'server.task.clear_notice',  # 设置是要将哪个任务进行定时
        'schedule': crontab(minute=0, hour=12),  # 调用crontab进行具体时间的定义
        # 'schedule': 3,  # 调用crontab进行具体时间的定义
        'args': ''
    },
}

# Load task modules from all registered Django app configs.
app.autodiscover_tasks([
    "server.task.clear_message",
    "server.task.clear_notice",
])
