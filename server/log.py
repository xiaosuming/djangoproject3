import os
import logging


_BASE_DIR = os.path.dirname(os.path.dirname(__file__))
_LOG = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '[%(asctime)s] %(levelname)s %(name)s: %(message)s'
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'filters': [],
                'formatter': 'standard',
            },
            'server_file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': 'INFO',
                'filename': os.path.join(_BASE_DIR, 'logs', 'server.log'),
                'formatter': 'standard',
                'maxBytes': 1024*1024*50,
                'backupCount': 5,
            },
            'error_file': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': 'WARNING',
                'filename': os.path.join(_BASE_DIR, 'logs', 'error.log'),
                'formatter': 'standard',
                'maxBytes': 1024*1024*50,
                'backupCount': 5,
            },
            'logstash': {
                'level': 'INFO',
                'class': 'logstash.TCPLogstashHandler',
                'host': os.getenv('DJANGO_LOG_SERVER_HOST', 'localhost'),
                'port': os.getenv('DJANGO_LOG_SERVER_PORT', 5000),
                'version': 1,
                'message_type': 'django',
            }
        },
        'loggers': {
            'server_info': {
                'handlers': ['console', 'server_file'],
                'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            },
            'server_error': {
                'handlers': ['console', 'error_file'],
                'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
            },
            'revproxy': {
                'handlers': ['console', 'server_file'],
                'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG')
            },
            'django': {
                'handlers': ['console', 'server_file'],
                'level': 'INFO',
                'propagate': True
            }
        },
    }


class Log:
    class __SetLogDir:
        def __init__(self):
            """初始化日志路径"""
            log_dir = os.path.join(_BASE_DIR, 'logs')
            if not os.path.exists(log_dir):
                os.makedirs(log_dir, exist_ok=True)
            self.LOGGING = _LOG

        def __get__(self, instance, owner):
            return self.LOGGING

    LOGGING = __SetLogDir()


class DotDict(dict):
    """
    dot.notation access to dictionary attributes
    描述符
    """
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


s_logger = DotDict({
    'glob': logging.getLogger('server_info'),
})
