from apps.application.models import Notice
from apps.chat.models import Message
import datetime
from celery import shared_task


# 消息超过90天，就进行删除
@shared_task
def clear_message():
    messages = Message.objects.all()
    # i = datetime.datetime.now()
    # d = (i + datetime.timedelta(days=-5)).strftime('%Y-%m-%d %H:%M:%S')
    for message in messages:
        # if d > message.send_time.strftime('%Y-%m-%d %H:%M:%S'):
            message.delete()
        # else:
        #     pass
    return "清除过期消息"


@shared_task
def clear_notice():
    notices = Notice.objects.all()
    # i = datetime.datetime.now()
    # d = (i + datetime.timedelta(days=-5)).strftime('%Y-%m-%d %H:%M:%S')
    for notice in notices:
        # if d > notice.send_time.strftime('%Y-%m-%d %H:%M:%S'):
        notice.delete()
        # else:
        #     pass
    return "清除过期应用通知"
